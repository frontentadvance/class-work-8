import React from "react";
import './app.css'
import Header from "../header/header";
import Info from '../info/info'

class App extends React.Component {
    state={
        films : [],
        people : [],
        planets : [],
        starships : []
    }

    onFilms = () => {
        reques('https://swapi.dev/api/films').then((data)=>{
            this.setState(()=>{
                return{
                    films : data.results
                }
            })
        })
    }

    onPeople = () => {
        reques('https://swapi.dev/api/people').then((data)=>{
            this.setState(()=>{
                return {
                    people : data.results
                }
            })
        })
    }

    onPlanets = () => {
        reques('https://swapi.dev/api/planets').then((data)=>{
            this.setState(()=>{
                return {
                    planets : data.results
                }
            })

        })
    }

    onStarships = () => {
        reques('https://swapi.dev/api/starships').then((data)=>{
            this.setState(()=>{
                return {
                    starships : data.results
                }
            })
        })
    }

    render(){
        console.log(this.state)
        return(
        <>
            <Header  
            onFilms = {this.onFilms} 
            onPeople = {this.onPeople} 
            onPlanets = {this.onPlanets} 
            onStarships = {this.onStarships}/>

            <Info 
            films = {this.state.films} 
            people = {this.state.people} 
            planets = {this.state.planets} 
            starships = {this.state.starships}/>
        </>
        )
    }
}

// https://swapi.dev/api/films
// function reques(url) {
//     const xml = new XMLHttpRequest()
//     xml.open('GET', url)
//     xml.onreadystatechange = () => {
//         if(xml.readyState === 4 && xml.status === 200){
//             return xml.responseText
//         }
//     }
//     xml.send()
// }

async function reques(url) {
    let data = await fetch(url)
    return data.json()
}



export default App