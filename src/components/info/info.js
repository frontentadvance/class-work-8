import React from "react";
import Card from '../card/card'
import './info.css'

function Info({films, people, planets, starships}) {
    return(<>
        <div className = 'info'>
        {films? films.map((element, index) => {
           return <Card key = {index*2+"f"} name = {element.title} planets = {element.planets} homeworld = {element.created}/> 
        }): null}
        {people? people.map((element, index)=>{
            return <Card key = {index*2+"a"} name = {element.name} planets = {`Рост: ${element.height}`} homeworld = {`Вес: ${element.mass}`}/>
        }):null}
        {planets? planets.map((element, index)=>{
            return <Card key = {index*2+"s"} name = {element.name} planets = {`Климат ${element.climate}`} homeworld = {`Местность: ${element.terrain}`}/>
        }):null}
        {starships? starships.map((element, index) => {
            return <Card key = {index*2+"d"} name = {element.name} planets = {`Модель ${element.model}`} homeworld = {`Класс корабля: ${element.starship_class}`}/>
        }):null}
        </div>
    </>)
}

export default Info