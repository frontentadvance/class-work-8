import React from "react";
import './header.css'
import Btns from "../btns/btns";

function Header({onFilms, onPeople, onPlanets, onStarships}) {
    return(
    <>
        <nav className = 'menu'>
            <ul className = 'nav'>
                <li className = 'nav__elem'><Btns name = "Films" onFilms = {onFilms}/></li>
                <li className = 'nav__elem'><Btns name = "Planets" onPlanets = {onPlanets}/></li>
                <li className = 'nav__elem'><Btns name = "Starships" onStarships = {onStarships}/></li>
                <li className = 'nav__elem'><Btns name = "people" onPeople = {onPeople}/></li>
            </ul>
        </nav>
    </>)
}

export default Header