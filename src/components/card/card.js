import React from "react";
import './card.css'

function Card({name, planets, homeworld}) {
   return(<>
        <article className = 'card'>
            <p className = "card-text name">{name}</p>
            <p className = 'card-text homeworld'>{homeworld}</p>
            <p className = 'card-text birth_year'>{Array.isArray(planets)?planets.map((element, index)=>{
                return (<><a href = {element} key = {index*2+"g"}>link {index + 1}</a><br/></>)
            }): planets}</p>
            <p className = 'card-text films'></p>
        </article>
   </>) 
}

export default Card