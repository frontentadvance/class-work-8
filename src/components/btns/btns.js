import React from "react";
import './btns.css'

function Btns({name,  onFilms, onPeople, onPlanets, onStarships}) {
    function clicked() {
        if(onFilms) {
            onFilms()
        }
        if(onPeople) {
            onPeople()
        }
        if(onPlanets) {
            onPlanets()
        }
        if(onStarships) {
            onStarships()
        }
    }
    return(<>
        <button className = 'btn' onClick={clicked}>{name}</button>
    </>)
}

export default Btns